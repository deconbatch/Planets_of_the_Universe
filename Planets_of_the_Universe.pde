/**
 * Planets of the Universe.
 *
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.04.12
 */

void setup() {
  size(980, 980, P3D);
  colorMode(HSB, 360, 100, 100, 100);
  sphereDetail(60);
  smooth();
}

void draw() {

  int   frmMax  = 3; // save 3 files
  float initHue = random(360.0);

  camera(700, 700, 700,
         0, 0, 0,
         0, 1, 0);
  lightFalloff(1.0, 0.001, 0.0);
  pointLight(0.0, 0.0, 100.0, 450.0, 200.0, 600.0);
  ambientLight(0.0, 0.0, 30.0);

  for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {
    background(0.0, 0.0, 0.0, 0.0);

    // draw background star field
    pushMatrix();
    rotateX(-HALF_PI * 0.5);
    rotateY(HALF_PI * 0.5);
    blendMode(ADD);
    starField(initHue + frmCnt * 120.0);
    blendMode(BLEND);
    drawNimbus();
    popMatrix();
 
    // draw planet
    blendMode(BLEND);
    drawPlanet(initHue + frmCnt * 120.0);
  
    saveFrame("frames/" + String.format("%04d", frmCnt + 1) + ".png");
  }

  exit();

}

/**
 * starField : draw star field.
 * @param  _initHue : star field color.
 */
private void starField(float _initHue) {

  int   ptnMax = 50; // draw 50 patterns
  float xPhase = random(1.0);
  float yPhase = random(1.0);
     
  for (int ptnCnt = 0; ptnCnt < ptnMax; ++ptnCnt) {
      
    float ptnRatio = map(ptnCnt, 0, ptnMax, 0.0, 1.0);

    int   plotMax = floor(random(5000.0, 20000.0));
    float plotDiv = 0.05;
    float baseHue = _initHue + map(ptnRatio, 0.0, 1.0, 0.0, 120.0);
    float baseSat = map(ptnRatio * ptnRatio, 0.0, 1.0, 40.0, 0.0);
    float baseBri = map(ptnRatio, 0.0, 1.0, 0.0, 40.0);
    float baseAlp = 100.0;
     
    // initial value of vector field calculation 
    float xPoint = cos(TWO_PI * (xPhase + ptnRatio)) * 0.3;
    float yPoint = sin(TWO_PI * (yPhase + ptnRatio)) * 0.3;
    float rPoint = 0.0;

    noFill();
    strokeWeight(0.05);
    stroke(baseHue % 360.0, baseSat, baseBri, baseAlp);
    beginShape();
    for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

      float plotRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);

      float xPrev = xPoint;
      float yPrev = yPoint;
      float rPrev = rPoint;

      // vector field calculation 
      rPoint += random(-1.0, 1.0);
      xPoint += plotDiv * cos(TWO_PI * rPoint) * plotRatio;
      yPoint += plotDiv * sin(TWO_PI * rPoint) * plotRatio;

      vertex(xPoint * 1.25 * width, yPoint * 1.25 * height, -1000.0);

    }
    endShape();

  }
}

/**
 * drawNimbus : draw nimbus in center of the canvas.
 */
private void drawNimbus() {

  float rMax = min(width, height) * 2.0;
  pushMatrix();
  translate(50.0, 0.0, 0.0);
  for (int i = 0; i < 20; i++) {
    float alp = map(i, 0, 20, 30.0, 0.0);
    float rad = map(i, 0, 20, rMax * 0.5, rMax);
    noStroke();
    fill(0.0, 0.0, 0.0, alp);
    ellipse(0.0, 0.0, rad, rad);
  }
  popMatrix();
  
}

/**
 * drawPlanet : draw planet.
 * @param  _initHue : planet color.
 */
private void drawPlanet(float _initHue) {

  float radius  = min(width, height) * 0.5;
  float divLat  = random(10.0, 20.0);
  int   pCntMax = floor(map(divLat, 10.0, 20.0, 20000, 10000));

  noStroke();
  fill(_initHue % 360.0, 5.0, 90.0, 100.0);
  sphere(radius);

  // latitude from 0 to 180 degrees
  for (float lat = 15.0; lat <= 100.0; lat += divLat) {
    float initLat = radians(lat);
    float divLon = 180 / constrain(((180 / divLat) * sin(initLat)), 0.5, 180.0 / divLat);

    // longitude from -50 to 130 degrees
    for (float lon = -50.0; lon <= 130.0; lon += divLon) {

      float initLon = radians(lon);
      float radLat  = initLat;
      float radLon  = initLon;

      float sHue    = _initHue + (((radLat + radLon) * 60.0) % 30.0) * 2.5;
      float sSat    = noise(radLat * 2.0, radLon * 2.0) * 60.0 + 20.0;
      float sBri    = noise(radLon * 2.0, radLat * 2.0) * 30.0 + 50.0;
      float baseSiz = random(2.0, 10.0);

      noStroke();
      fill(sHue % 360.0, sSat, sBri, 10.0);
      for (int pCnt = 0; pCnt < pCntMax; pCnt++) {

        float pCntRatio = map(pCnt, 0, pCntMax, 0.0, 1.0);

        float prevLat = radLat;
        float prevLon = radLon;

        radLat += random(-0.001, 0.001) * PI;
        radLon += random(-0.001, 0.001) * TWO_PI;
        radLat = radLat % PI;
        radLon = radLon % TWO_PI;
       
        float cX = radius * cos(radLon) * sin(radLat);
        float cY = radius * sin(radLon) * sin(radLat);
        float cZ = radius * cos(radLat);
     
        pushMatrix();
        translate(cX, cY, cZ);
        // must be Z -> Y 
        rotateZ(radLon);
        rotateY(radLat);
        float eSiz = 0.0 + sin(PI * pCntRatio) * baseSiz;
        ellipse(0.0, 0.0, eSiz, eSiz);     
        popMatrix();

      }
    }
  } 
}
